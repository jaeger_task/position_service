ALTER TABLE IF EXISTS "position_attributes" DROP CONSTRAINT IF EXISTS "position_attributes_position_id_fkey" ;

ALTER TABLE IF EXISTS "position_attributes" DROP CONSTRAINT IF EXISTS "position_attributes_attribute_id_fkey" ;

ALTER TABLE IF EXISTS "position" DROP CONSTRAINT IF EXISTS "position_profession_id_fkey";

DROP TABLE IF  EXISTS "position_attributes";

DROP TABLE IF  EXISTS "attribute" ;

DROP TABLE IF  EXISTS "position";

DROP TABLE IF  EXISTS "profession";

DROP TYPE IF  EXISTS "attribute_types";


