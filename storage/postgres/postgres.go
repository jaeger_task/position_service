package postgres

import (
	"context"
	"github.com/Ramazon1227/position_service/config"
	"github.com/Ramazon1227/position_service/storage"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/opentracing/opentracing-go"
)

type Store struct {
	db         *Pool
	profession storage.ProfessionI
	attribute  storage.AttributeI
	position   storage.PositionI
}

type Pool struct {
	db *pgxpool.Pool
}

func (b *Pool) QueryRow(ctx context.Context, sql string, args ...any) pgx.Row {
	dbSpan, ctx := opentracing.StartSpanFromContext(ctx, "pgx.QueryRow")
	defer dbSpan.Finish()

	dbSpan.SetTag("sql", sql)
	dbSpan.SetTag("args", args)

	return b.db.QueryRow(ctx, sql, args...)
}

func (b *Pool) Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error) {
	dbSpan, ctx := opentracing.StartSpanFromContext(ctx, "pgx.Query")
	defer dbSpan.Finish()

	dbSpan.SetTag("sql", sql)
	dbSpan.SetTag("args", args)

	return b.db.Query(ctx, sql, args...)
}

func (b *Pool) Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error) {
	dbSpan, ctx := opentracing.StartSpanFromContext(ctx, "pgx.Exec")
	defer dbSpan.Finish()

	dbSpan.SetTag("sql", sql)
	dbSpan.SetTag("args", arguments)

	return b.db.Exec(ctx, sql, arguments...)
}

func NewPostgres(psqlConnString string, cfg config.Config) (storage.StorageI, error) {
	// First set up the pgx connection pool
	config, err := pgxpool.ParseConfig(psqlConnString)
	if err != nil {
		return nil, err
	}

	config.AfterConnect = nil
	config.MaxConns = int32(cfg.PostgresMaxConnections)

	pool, err := pgxpool.ConnectConfig(context.Background(), config)
	
    
	dbPool := &Pool{
		db: pool,
	}
	return &Store{
		db: dbPool,
	}, err
}

func (s *Store) Profession() storage.ProfessionI {
	if s.profession == nil {
		s.profession = NewProfessionRepo(s.db)
	}
	return s.profession
}

func (s *Store) Attribute() storage.AttributeI {
	if s.attribute == nil {
        s.attribute = NewAttributeRepo(s.db)
	}
	return s.attribute
}

func (s *Store) Position() storage.PositionI {
	if s.position == nil {
        s.position = NewPositionRepo(s.db)
	}
	return s.position
}