package postgres

import (
	"context"
	"fmt"

	"github.com/Ramazon1227/position_service/pkg/helper"
	"github.com/Ramazon1227/position_service/storage"

	pb "github.com/Ramazon1227/position_service/genproto/position_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/opentracing/opentracing-go"
)
type positionRepo struct {
	db *Pool
}

func NewPositionRepo(db *Pool) storage.PositionI {
	return &positionRepo{
		db: db,
	}
}

func (r *positionRepo) CreatePosition(ctx context.Context, req *pb.CreatePositionRequest) (resp *pb.PositionId, err error) {


	dbSpan, ctx := opentracing.StartSpanFromContext(ctx, "positionStorage.Create")
	defer dbSpan.Finish()
	tx, err := r.db.db.BeginTx(ctx,pgx.TxOptions{})
	if err != nil {
		return nil, err
	}


	insertPositionQuery := `INSERT INTO position(id,name,profession_id,company_id) VALUES ($1,$2,$3,$4) returning id`

	positionId := uuid.New().String()

	raw, err := r.db.Exec(ctx, insertPositionQuery, positionId, req.Name, req.ProfessionId, req.CompanyId)

	if err != nil {
		return nil, fmt.Errorf("error while creating position %w",err)
	}
	
	if raw.RowsAffected() != 1{
		return nil,fmt.Errorf("error while creating position: invalid parametrs")
	}
	for _, att := range req.PositionAttributes {

		attId,err := r.CreatePosAttribute(ctx,&pb.CreatePosAttributeRequest{
			AttributeId:att.AttributeId ,
			PositionId: positionId,
			Value: att.Value,
		})
		if err != nil {
			tx.Rollback(ctx)
			return nil, fmt.Errorf("error while creating position attributes %w",err)
		}
		if attId != "" {
			tx.Rollback(ctx)
			return nil, fmt.Errorf("error while creating position attributes ")
		}

	}
	
	tx.Commit(ctx)
	return &pb.PositionId{
		Id: positionId,
	}, err
}

func (r *positionRepo) GetAllPosition(ctx context.Context, req *pb.GetAllPositionRequest) (*pb.GetAllPositionResponse, error) {
	dbSpan, ctx := opentracing.StartSpanFromContext(ctx, "positionStorage.GetAll")
	defer dbSpan.Finish()
	var (
		resp   pb.GetAllPositionResponse
		err    error
		filter string
		params = make(map[string]interface{})
	)

	if req.Search != "" {
		filter += " AND name ILIKE '%' || :search || '%' "
		params["search"] = req.Search
	}

	if req.ProfessionId != "" {
		filter += " AND profession_id=:profession_id"
		params["profession_id"] = req.ProfessionId
	}

	if req.CompanyId != "" {
		filter += " AND company_id=:company_id"
		params["company_id"] = req.CompanyId
	}
	countQuery := `SELECT count(1) FROM position WHERE true ` + filter

	q, arr := helper.ReplaceQueryParams(countQuery, params)
	err = r.db.QueryRow(ctx, q, arr...).Scan(
		&resp.Count,
	)

	if err != nil {
		return nil, fmt.Errorf("error while scanning count %w", err)
	}

	positionIdQuery := `SELECT id FROM position WHERE true ` + filter
	q, arr = helper.ReplaceQueryParams(positionIdQuery, params)

	raws,err := r.db.Query(ctx,q,arr...)
	for raws.Next() { 
		var positionId pb.PositionId
		err = raws.Scan(
			&positionId.Id,
		)
		if err != nil {
			return nil, fmt.Errorf("error while scanning positionId err: %w", err)
		}
		position,err:=r.GetPosition(ctx,&positionId)
        if err != nil {
			return nil, fmt.Errorf("error while getting position err: %w", err)
		}
		resp.Positions = append(resp.Positions,position)

}

	return &resp, err
}

func (r *positionRepo) GetPosition(ctx context.Context, req *pb.PositionId) (*pb.Position, error) {
	dbSpan, ctx := opentracing.StartSpanFromContext(ctx, "positionStorage.Get")
	defer dbSpan.Finish()
	var position pb.Position
	queryPos := `SELECT * from position WHERE id = $1`

	 err := r.db.QueryRow(ctx, queryPos, req.Id).Scan(
		&position.Id,
		&position.Name,
		&position.ProfessionId,
		&position.CompanyId,

	 )
	if err != nil {
		return nil, fmt.Errorf("error while getting position ")
	}

     attributes,err := r.GetPosAttributes(ctx,req)
	 if err != nil{
		return nil, fmt.Errorf("error while getting position attributes")
	 }
	 fmt.Println(attributes)
	 position.PositionAttributes=attributes
	return &position, nil

}

func (r *positionRepo) UpdatePosition(ctx context.Context, req *pb.UpdatePositionRequest) (*pb.PositionId, error) {
	dbSpan, ctx := opentracing.StartSpanFromContext(ctx, "positionStorage.Update")
	defer dbSpan.Finish()
	tx, err := r.db.db.Begin(ctx)

	if err != nil {
		return nil, err
	}
	updatePositionQuery := `UPDATE position
	SET name = $1 , profession_id = $2, company_id = $3 
	WHERE id = $4`

	positionId := req.Id

	raw, err := tx.Exec(ctx, updatePositionQuery,  req.Name, req.ProfessionId, req.CompanyId,positionId)
    
	if err != nil || raw.RowsAffected()!=1 {
		return nil, fmt.Errorf("error while updating position ")
	}
	 _,err = r.db.Exec(ctx,`CREATE TABLE IF NOT exists updated_attributes("id" uuid)`)
	 if err != nil {
			tx.Rollback(ctx)
			return nil, fmt.Errorf("error while updating position attributes create table %w",err)
		}
	for _, att := range req.PositionAttributes {

		Id, err := r.UpdatePosAttribute(ctx,&pb.CreatePosAttributeRequest{
			Value: att.Value,
			AttributeId: att.AttributeId,
			PositionId: positionId,

		 })
      
		if err != nil {
			tx.Rollback(ctx)
			return nil, fmt.Errorf("error while updating position attributes %w",err)
		}
		_,err = r.db.Query(ctx,`INSERT INTO updated_attributes(id) VALUES ($1)`,Id)
		if err != nil {
			tx.Rollback(ctx)
			return nil, fmt.Errorf("error while updating position attributes insertt into err :%w",err)
		}
	}
	deleteQuery:=`DELETE FROM position_attributes  pos_at
      WHERE pos_at.position_id = $1 AND pos_at.id NOT IN 
     (SELECT up_at.id
    FROM updated_attributes  up_at )`
	_,err = r.db.Exec(ctx,deleteQuery,positionId)
	if err != nil {
			tx.Rollback(ctx)
			return nil, fmt.Errorf("error while updating position attributes delete err: %w",err)
		}
		_,err = r.db.Exec(ctx,`DROP TABLE IF exists updated_attributes`)
	 if err != nil {
			tx.Rollback(ctx)
			return nil, fmt.Errorf("error while updating position attributes drop table: %w",err)
		}
        tx.Commit(ctx)
	return &pb.PositionId{Id: positionId}, err
}

func (r *positionRepo) DeletePosition(ctx context.Context, req *pb.PositionId) (err error) {

	dbSpan, ctx := opentracing.StartSpanFromContext(ctx, "positionStorage.Delete")
	defer dbSpan.Finish()
	query := `DELETE FROM position WHERE id = $1`

	raw, err := r.db.Exec(ctx, query, req.Id)

	if err != nil {
		return fmt.Errorf("error while deleting position")
	}

	if raw.RowsAffected() != 1 {
		return fmt.Errorf("position not found")
	}

	return nil
}

