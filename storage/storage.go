package storage

import (
	"context"
	"errors"
	pb "github.com/Ramazon1227/position_service/genproto/position_service"
)

var ErrorTheSameId = errors.New("cannot use the same uuid for 'id' and 'parent_id' fields")
var ErrorProjectId = errors.New("not valid 'project_id'")

type StorageI interface {
	Profession() ProfessionI
	Attribute()  AttributeI
	Position()   PositionI
}

type ProfessionI interface {
	Create(ctx context.Context, entity *pb.CreateProfessionRequest) (id string, err error)
	GetAll(ctx context.Context, req *pb.GetAllProfessionRequest) (*pb.GetAllProfessionResponse, error)
    GetByID(ctx context.Context,req *pb.GetByIDProfessionRequest) (*pb.Profession,error)
	DeleteByID(ctx context.Context,req *pb.GetByIDProfessionRequest) (error)
	UpdateByID(ctx context.Context,req *pb.Profession) (error)
}

type AttributeI interface {
	Create(ctx context.Context, entity *pb.CreateAttributeRequest)(id string , err error)
	// GetAll(ctx context.Context, req *pb.GetAllAttributRequest) (*pb.GetAllAttributResponse, error) 
	Get(ctx context.Context,req *pb.AttributeId) (*pb.Attribute,error)
	Delete(ctx context.Context,entity *pb.AttributeId) (error)
	Update(ctx context.Context,req *pb.Attribute) (error)
}

type PositionI interface {
	CreatePosition(ctx context.Context, req *pb.CreatePositionRequest)(resp *pb.PositionId,err error)
	GetAllPosition(ctx context.Context,req *pb.GetAllPositionRequest)(*pb.GetAllPositionResponse,error)
	GetPosition(ctx context.Context,req *pb.PositionId)(*pb.Position,error)
	UpdatePosition(ctx context.Context, req *pb.UpdatePositionRequest)(*pb.PositionId,error)
	DeletePosition(ctx context.Context, req *pb.PositionId)(err error)
}