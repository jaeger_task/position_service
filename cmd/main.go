package main

import (
	"fmt"
	"net"

	"github.com/Ramazon1227/position_service/config"
	"github.com/Ramazon1227/position_service/grpc"
	"github.com/Ramazon1227/position_service/grpc/service"
	"github.com/Ramazon1227/position_service/pkg/logger"
	"github.com/Ramazon1227/position_service/storage/postgres"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	jaeger_config "github.com/uber/jaeger-client-go/config"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)


	jaegerCfg := &jaeger_config.Configuration{
		ServiceName: cfg.ServiceName,

		// "const" sampler is a binary sampling strategy: 0=never sample, 1=always sample.
		Sampler: &jaeger_config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},

		// Log the emitted spans to stdout.
		Reporter: &jaeger_config.ReporterConfig{
			LogSpans:           true,
			LocalAgentHostPort: cfg.JaegerHostPort,
		},
	}
    tracer, closer, err := jaegerCfg.NewTracer(jaeger_config.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	pgStore, err := postgres.NewPostgres(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	), cfg)
	if err != nil {
		panic(err)
	}
    
	grpcClients,err:=service.NewGrpcClients(&cfg)
	if err != nil {
		panic(err)
	}
	grpcServer := grpc.SetUpServer(cfg, log, pgStore,grpcClients)
	lis, err := net.Listen("tcp", cfg.GRPCPort)
	if err != nil {
		panic(err)
	}

	log.Info("GRPC: Server being started...", logger.String("port", cfg.GRPCPort))

	if err := grpcServer.Serve(lis); err != nil {
		panic(err)
	}

}