package grpc

import (
	"github.com/Ramazon1227/position_service/config"
	"github.com/Ramazon1227/position_service/genproto/position_service"
	"github.com/Ramazon1227/position_service/grpc/service"
	"github.com/Ramazon1227/position_service/pkg/logger"
	"github.com/Ramazon1227/position_service/storage"
	otgrpc "github.com/opentracing-contrib/go-grpc"
	"github.com/opentracing/opentracing-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI,serv service.ServiceManager) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer(
		grpc.UnaryInterceptor(
			otgrpc.OpenTracingServerInterceptor(opentracing.GlobalTracer())),
		grpc.StreamInterceptor(
			otgrpc.OpenTracingStreamServerInterceptor(opentracing.GlobalTracer())),
	)

	position_service.RegisterProfessionServiceServer(grpcServer, service.NewProfessionService(cfg, log, strg))
	position_service.RegisterAttributeServiceServer(grpcServer,service.NewAttributeService(cfg,log,strg))
	position_service.RegisterPositionServiceServer(grpcServer,service.NewPositionService(cfg,log,strg,&serv))
	reflection.Register(grpcServer)
	return
}