package service

import (
	"fmt"
	"github.com/Ramazon1227/position_service/config"
	"github.com/Ramazon1227/position_service/genproto/company_service"
	otgrpc "github.com/opentracing-contrib/go-grpc"
	"github.com/opentracing/opentracing-go"
	"google.golang.org/grpc"
)


type ServiceManager interface {
	CompanyService() company_service.CompanyServiceClient
}

type grpcClients struct {
	company_service company_service.CompanyServiceClient
}

func (g *grpcClients) CompanyService() company_service.CompanyServiceClient {
	return g.company_service
}

func NewGrpcClients(conf *config.Config) (ServiceManager,error){

	companyService,err := grpc.Dial(
		fmt.Sprintf("%s:%d",conf.CompanyServiceHost,conf.CompanyServicePort),
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(
			otgrpc.OpenTracingClientInterceptor(opentracing.GlobalTracer())),
		grpc.WithStreamInterceptor(
			otgrpc.OpenTracingStreamClientInterceptor(opentracing.GlobalTracer())),
		
	)
	if err!=nil {
		return nil, err
	}

	return &grpcClients{
		company_service: company_service.NewCompanyServiceClient(companyService),
	},nil
}