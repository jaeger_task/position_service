package service

import (
	"context"
	"github.com/Ramazon1227/position_service/storage"
	"github.com/Ramazon1227/position_service/config"
	pb "github.com/Ramazon1227/position_service/genproto/position_service"
	"github.com/Ramazon1227/position_service/pkg/logger"
	"google.golang.org/protobuf/types/known/emptypb"
)

type attributeService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	pb.UnimplementedAttributeServiceServer
}

func NewAttributeService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) *attributeService {
	return &attributeService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}


func (s *attributeService) Create(ctx context.Context, req *pb.CreateAttributeRequest) (*pb.AttributeId,error) {
	id, err := s.strg.Attribute().Create(ctx,req)
	if err != nil {
		s.log.Error("CreateAttribute", logger.Any("req", req), logger.Error(err))
		return nil, err
	}

	return &pb.AttributeId{
		Id:   id,
	}, nil
}


// func (s *attributeService) GetAll(ctx context.Context, req *pb.GetAllAttributRequest) (*pb.GetAllAttributResponse, error) {
// 	resp, err := s.strg.Attribute().GetAll(ctx, req)
// 	if err != nil {
// 		s.log.Error("GetAllAttribute", logger.Any("req", req), logger.Error(err))
// 		return nil, err
// 	}
// 	return resp, nil
// }


func (s *attributeService) Get(ctx context.Context,req *pb.AttributeId) (*pb.Attribute,error){
    resp, err :=s.strg.Attribute().Get(ctx,req)
    if err != nil {
		s.log.Error("Get attribute", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	return resp, nil	
}


func (s *attributeService) Delete(ctx context.Context,req *pb.AttributeId) (*emptypb.Empty,error){
    err:=s.strg.Attribute().Delete(ctx,req)
    if err != nil {
		s.log.Error("Delete Attribute", logger.Any("req", req), logger.Error(err))
		return &emptypb.Empty{},err
	}
	return  &emptypb.Empty{},err
}


func (s *attributeService) Update(ctx context.Context,req *pb.Attribute) (*pb.AttributeId,error){
  
	err:=s.strg.Attribute().Update(ctx,req)
    if err != nil {
		s.log.Error("Update Attribute", logger.Any("req", req), logger.Error(err))
		return nil,err
	}
	
	 return  &pb.AttributeId{
		Id:req.Id ,
	}, nil

}