package service

import (
	"context"
	"fmt"
	"github.com/Ramazon1227/position_service/config"
	"github.com/Ramazon1227/position_service/storage"

	pb "github.com/Ramazon1227/position_service/genproto/position_service"
	"github.com/Ramazon1227/position_service/pkg/logger"

	"github.com/Ramazon1227/position_service/genproto/company_service"
	"google.golang.org/protobuf/types/known/emptypb"
)


type positionService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	service ServiceManager
	pb.UnimplementedPositionServiceServer
}

func NewPositionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI,service *ServiceManager) *positionService {
	return &positionService{
		cfg:  cfg,
		log:  log,
		strg: strg,
		service: *service,
	}
}

func (s *positionService) CreatePosition(ctx context.Context, req *pb.CreatePositionRequest )(resp *pb.PositionId,err error){
   

	_,err = s.service.CompanyService().Get(ctx,
	&company_service.CompanyId{
		Id: req.CompanyId,
	})
	if err != nil {
		s.log.Error("Create Position", logger.Any("req", req), logger.Error(err))
		return nil,fmt.Errorf("error while creating position: invalid company id")
	}	
	resp,err = s.strg.Position().CreatePosition(ctx, req)
	if err != nil {
		s.log.Error("Create Position", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	
     return resp,err;

}

func (s *positionService) GetAllPosition(ctx context.Context, req *pb.GetAllPositionRequest)(*pb.GetAllPositionResponse,error){
   
	if req.CompanyId != ""{
		_,err := s.service.CompanyService().Get(ctx,
			&company_service.CompanyId{
				Id: req.CompanyId,
			})
		if err != nil {
			s.log.Error("Get all Position", logger.Any("req", req), logger.Error(err))
		   return nil, err
		}	
		}
	
	resp,err := s.strg.Position().GetAllPosition(ctx, req)
	if err != nil {
		s.log.Error("Get all Position", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	
     return resp,err;

}

func (s *positionService) GetPosition(ctx context.Context,req *pb.PositionId)(resp *pb.Position,err error){
   
	resp,err = s.strg.Position().GetPosition(ctx, req)
	if err != nil {
		s.log.Error("Get Position", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	return resp, nil
}


func (s *positionService) UpdatePosition(ctx context.Context,req *pb.UpdatePositionRequest) (*pb.PositionId,error){
  
	_,err := s.service.CompanyService().Get(ctx,
		&company_service.CompanyId{
			Id: req.CompanyId,
		})
	if err != nil {
		s.log.Error("Update Position", logger.Any("req", req), logger.Error(err))
		return nil,err
	}	
	_,err =s.strg.Position().UpdatePosition(ctx,req)
    if err != nil {
		s.log.Error("Update Position", logger.Any("req", req), logger.Error(err))
		return nil,err
	}
	
	 return  &pb.PositionId{
		Id:req.Id ,
	}, nil

}


func (s *positionService) DeletePosition(ctx context.Context,req *pb.PositionId) (*emptypb.Empty,error){
    err:=s.strg.Position().DeletePosition(ctx,req)
    if err != nil {
		s.log.Error("Delete Position", logger.Any("req", req), logger.Error(err))
		return &emptypb.Empty{},err
	}
	return  &emptypb.Empty{},err
}