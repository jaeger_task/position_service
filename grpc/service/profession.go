package service

import (
	"context"
	"github.com/Ramazon1227/position_service/config"
	pb "github.com/Ramazon1227/position_service/genproto/position_service"
	"github.com/Ramazon1227/position_service/pkg/logger"
	"github.com/Ramazon1227/position_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)

type professionService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	pb.UnimplementedProfessionServiceServer
}

func NewProfessionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) *professionService {
	return &professionService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s *professionService) Create(ctx context.Context, req *pb.CreateProfessionRequest) (*pb.Profession, error) {
	id, err := s.strg.Profession().Create(ctx, req)
	if err != nil {
		s.log.Error("CreateProfession", logger.Any("req", req), logger.Error(err))
		return nil, err
	}

	return &pb.Profession{
		Id:   id,
		Name: req.Name,
	}, nil
}

func (s *professionService) GetAll(ctx context.Context, req *pb.GetAllProfessionRequest) (*pb.GetAllProfessionResponse, error) {
	resp, err := s.strg.Profession().GetAll(ctx, req)
	if err != nil {
		s.log.Error("GetAllProfession", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	return resp, nil
}

func (s *professionService) GetByID(ctx context.Context,req *pb.GetByIDProfessionRequest) (*pb.Profession,error){
    resp, err :=s.strg.Profession().GetByID(ctx,req)
    if err != nil {
		s.log.Error("Get profession ", logger.Any("req", req), logger.Error(err))
		return nil, err
	}
	return resp, nil
}

func (s *professionService) DeleteByID(ctx context.Context,req *pb.GetByIDProfessionRequest) (*emptypb.Empty,error){
    err:=s.strg.Profession().DeleteByID(ctx,req)
    if err != nil {
		s.log.Error("Delete Profession", logger.Any("req", req), logger.Error(err))
		return &emptypb.Empty{},err
	}
	return  &emptypb.Empty{},err
}

func (s *professionService) UpdateByID(ctx context.Context,req *pb.Profession) (*pb.Profession,error){
  
	err:=s.strg.Profession().UpdateByID(ctx,req)
    if err != nil {
		s.log.Error("UpdateByID", logger.Any("req", req), logger.Error(err))
		return nil,err
	}
	profession,_:=s.strg.Profession().GetByID(ctx,&pb.GetByIDProfessionRequest{Id:req.Id})
	return profession, nil

}